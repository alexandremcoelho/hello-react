import logo from "./logo.svg";
import "./App.css";
import { Text } from "./components/Text";

function App() {
  return (
    <h1>
      Hello, <Text color="green" value="Kenzie" />
      <div>
        Hello, <Text color="blue" value="alexandre coelho" />
      </div>
    </h1>
  );
}

export default App;
